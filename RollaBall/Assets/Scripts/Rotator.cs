﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour {

    // Update is called once per frame
    public PlayerController m_Player;

	void Update () {
        transform.Rotate(new Vector3(30, 40, 50) * Time.deltaTime);
	}

    private void OnTriggerEnter(Collider other) {
        if(other.tag == "Player")
        {
            m_Player.IncreaseCount();
            gameObject.SetActive(false);
        }
    }
}
