﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public string m_AxisH;
    public string m_AxisV;
    public float m_Velocity;
    public Text m_ScoreBoard;
    public Text m_WinText;
    public AudioSource m_Audio;
    protected int m_Count = -1;

    // Use this for initialization
    void Start () {
        IncreaseCount();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        float MovementH = Input.GetAxis(m_AxisH);
        float MovementV = Input.GetAxis(m_AxisV);
        Vector3 Force = new Vector3(MovementH, 0.0f, MovementV);
        GetComponent<Rigidbody>().AddForce(Force * m_Velocity);
    }

    public void IncreaseCount() {
        if (m_Count != -1)
        {
            m_Audio.Play();
        }
        m_Count++;
        m_ScoreBoard.text = "Count: " + m_Count;
        if(m_Count == 8)
        {
            m_WinText.text = "Hey, that was pretty good";
        }
    }
}
