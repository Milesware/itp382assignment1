﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject m_Player;
    private Vector3 m_Offset;

	// Use this for initialization
	void Start () {
        m_Offset = transform.position - m_Player.transform.position;
    }
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = m_Player.transform.position + m_Offset;
	}
}
